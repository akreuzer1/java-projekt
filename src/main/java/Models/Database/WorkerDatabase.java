package Models.Database;

import Models.Pensioner;
import Models.Student;
import Models.Worker;
import Models.WorkerRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class WorkerDatabase implements WorkerRepository {
    static Logger log = Logger.getLogger("Models/Database/WorkerDatabase.java");

    List<Worker> database = new ArrayList<Worker>();


    public WorkerDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("WorkerDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        database.add(new Worker(UUID.randomUUID(), "Bruno", "Petković", 30000,40,42,true));
        database.add(new Worker(UUID.randomUUID(), "Perica", "Perić", 38900,22,3,true));
        database.add(new Worker(UUID.randomUUID(), "Narko", "Nyarko", 45000,35,3,false));


    }

    @Override
    public Worker get(UUID id) {
        for(Worker w :database){
            if(w.getUuid().compareTo(id)==0){
                return w;
            }
        }
        return null;
    }

    @Override
    public void add(Worker worker) {
        database.add(worker);

    }

    @Override
    public boolean update(Worker worker) {
        for (Worker s : database){
            if(s.getUuid() == worker.getUuid()){
                s.setFirstName(worker.getFirstName());
                s.setLastname(worker.getLastname());
                s.setAccountBalance(worker.getAccountBalance());
                s.setChildernNumber(worker.getChildernNumber());
                s.setSalaryInput(worker.getSalaryInput());

                return true;
            }
        }
        database.add(worker);
        return false;

    }

    @Override
    public void remove(Worker worker) {
        database.remove(worker);

    }

    @Override
    public List<Worker> getAllWorkers() {
        log.warning("getAllWorkers have been called!");
        return database;
    }
}

package Models.Database;

import Models.Student;
import Models.StudentRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class StudentDatabase implements StudentRepository {
    static Logger log = Logger.getLogger("Models/Database/StudentDatabase.java");

    List<Student> database = new ArrayList<Student>();


    public StudentDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("StudentDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        database.add(new Student(UUID.randomUUID(), "Bruno", "Petković", 30000,40));
        database.add(new Student(UUID.randomUUID(), "Perica", "Perić", 38900,22));
        database.add(new Student(UUID.randomUUID(), "Narko", "Nyarko", 45000,35));


    }

    @Override
    public Student get(UUID id) {
        for(Student s :database){
            if(s.getUuid().compareTo(id)==0){
                return s;
            }
        }
        return null;
    }

    @Override
    public void add(Student student) {
        database.add(student);
    }

    @Override
    public boolean update(Student student) {
        for (Student s : database){
            if(s.getUuid() == student.getUuid()){
                s.setFirstName(student.getFirstName());
                s.setLastname(student.getLastname());
                s.setAccountBalance(student.getAccountBalance());
                s.setHourlyRate(student.getHourlyRate());
                return true;
            }
        }
        database.add(student);
        return false;
    }

    @Override
    public void remove(Student student) {
        database.remove(student);

    }

    @Override
    public List<Student> getAllStudents() {
        log.warning("GetAllPensioners have been called!");
        return database;
    }
}

module com.example.loginprozor {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;


    opens com.example.loginprozor to javafx.fxml;
    exports com.example.loginprozor;
}
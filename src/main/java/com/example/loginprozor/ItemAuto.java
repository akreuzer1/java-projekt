package com.example.loginprozor;

import java.util.UUID;

public class ItemAuto {
    protected UUID uuid;
    protected String model;
    protected String marka;
    protected double km;
    protected int god;
    protected double cijena;




    ItemAuto(UUID uuid, String marka, String model, double cijena, double km, int god  ){
        this.uuid = uuid;
        this.cijena = cijena;
        this.marka = marka;
        this.model = model;
        this.km = km;
        this.god = god;

    }




    public String getMarka() {
        return marka;
    }
    public String getModel() {
        return model;
    }
    public double getCijena() {
        return cijena;
    }
    public double getKm() {
        return km;
    }
    public int getGod() {
        return god;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setKm(double model) {
        this.km = km;
    }
    public void setGod(int god) {
        this.god = god;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }



}

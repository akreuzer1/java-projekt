package com.example.loginprozor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class AutoHandler implements AutoFunkcije {
    static Logger log = Logger.getLogger(AutoHandler.class.getName());
    List<Auto> auti = new ArrayList<Auto>();

    public AutoHandler(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("AutoHandler.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        Auto auto1 = new Auto(UUID.randomUUID(),"BMW","serija3",1233.23,122000,2013);
        Auto auto2 = new Auto(UUID.randomUUID(),"Renault","megane",23213,32000,2010);
        Auto auto3 = new Auto(UUID.randomUUID(),"Audi","R8",10233.23,2333,2022);

        auti.add(auto1);
        auti.add(auto2);
        auti.add(auto3);
    }

    @Override
    public List<Auto> getAuto() {
        return auti;
    }



    @Override
    public void add(Auto auto) {
        auti.add(auto);
    }


    @Override
    public void remove(Auto auto) {
        auti.remove(auto);

    }


    @Override
    public boolean update(Auto auto) {
        for (Auto s : auti){
            if(s.getUuid() == auto.getUuid()){
                s.setMarka(auto.getMarka());
                s.setModel(auto.getModel());
                s.setKm(auto.getKm());
                s.setGod(auto.getGod());
                s.setCijena(auto.getCijena());
                return true;
            }
        }
        auti.add(auto);
        return false;
    }

}

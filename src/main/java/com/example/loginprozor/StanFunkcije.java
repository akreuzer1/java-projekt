package com.example.loginprozor;

import java.util.List;

public interface StanFunkcije {

    List<Stan> getStan();
    void add(Stan stan);

    void remove(Stan stan);

    boolean update(Stan stan);
}

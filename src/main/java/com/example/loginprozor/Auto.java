package com.example.loginprozor;

import java.util.UUID;

public class Auto extends ItemAuto implements iPrikazi {

    @Override
    public String prikaziOglas(){return marka+";"+ model + ";" + km + ";" + god + ";" +  cijena+ ";"+ uuid;}

    public Auto(UUID uuid,  String marka,String model, double cijena, double km, int god ){
        super(uuid,marka, model, cijena, km, god);
        this.marka = marka;
        this.model = model;
        this.km = km;
        this.god = god;
        this.cijena = cijena;

    }

    @Override
    public String toString(){
        return "Marka="+marka+"\n"+"Model="+model+"\n"+"KM="+km+"\n"+"Godište="+god+"\n"+"Cijena="+cijena+"\n";
    }


}

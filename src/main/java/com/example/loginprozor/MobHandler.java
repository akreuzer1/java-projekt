package com.example.loginprozor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MobHandler implements MobFunkcije {
    static Logger log = Logger.getLogger(MobHandler.class.getName());
    List<Mob> mobiteli = new ArrayList<>();

    public MobHandler(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("MobHandler.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        Mob mob1 = new Mob(UUID.randomUUID(),"Samsung","s22",500);
        Mob mob2 = new Mob(UUID.randomUUID(),"Žaomi","Para",200);
        Mob mob3 = new Mob(UUID.randomUUID(),"Ajfon","50",1000);

        mobiteli.add(mob1);
        mobiteli.add(mob2);
        mobiteli.add(mob3);
    }

    @Override
    public List<Mob> getMob() {
        return mobiteli;
    }



    @Override
    public void add(Mob mob) {
        mobiteli.add(mob);
    }


    @Override
    public void remove(Mob mob) {
        mobiteli.remove(mob);

    }


    @Override
    public boolean update(Mob mob) {
        for (Mob m : mobiteli){
            if(m.getUuidM() == mob.getUuidM()){
                m.setMarka(mob.getMarka());
                m.setModel(mob.getModel());
                m.setCijena(mob.getCijena());
                return true;
            }
        }
        mobiteli.add(mob);
        return false;
    }
}

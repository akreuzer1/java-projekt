package com.example.loginprozor;

import java.util.UUID;

public class ItemMob {
    protected UUID uuid;
    protected String model;
    protected String marka;

    protected double cijena;




    ItemMob(UUID uuid, String marka, String model, double cijena ){
        this.uuid = uuid;
        this.cijena = cijena;
        this.marka = marka;
        this.model = model;


    }




    public String getMarka() {
        return marka;
    }
    public String getModel() {
        return model;
    }
    public double getCijena() {
        return cijena;
    }

    public UUID getUuidM() {
        return uuid;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public void getUuidM(UUID uuid) {
        this.uuid = uuid;
    }
}

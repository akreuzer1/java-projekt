package com.example.loginprozor;

import java.util.UUID;

public class ItemStan {
    protected UUID uuid;
    protected int kvadratura;
    protected String mjesto;
    protected String ulica;
    protected double cijena;
    protected int etaza;




    ItemStan(UUID uuid, int kvadratura, String mjesto, String ulica, double cijena, int etaza  ){
        this.uuid = uuid;
        this.kvadratura = kvadratura;
        this.mjesto = mjesto;
        this.ulica = ulica;
        this.cijena = cijena;
        this.etaza = etaza;

    }




    public int getKvadrat() {
        return kvadratura;
    }
    public String getMjesto() {
        return mjesto;
    }
    public double getCijena() {
        return cijena;
    }
    public String getUlica() {
        return ulica;
    }
    public int getEtaza() {
        return etaza;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public void setKvadratura(int kvadratura) {
        this.kvadratura = kvadratura;
    }

    public void setMjesto(String mjesto) {
        this.mjesto = mjesto;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }
    public void setEtaza(int etaza) {
        this.etaza = etaza;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

}

package com.example.loginprozor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class StanHandler implements StanFunkcije{

    static Logger log = Logger.getLogger(StanHandler.class.getName());
    List<Stan> stanovi = new ArrayList<Stan>();

    public StanHandler(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("StanHandler.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        Stan stan1 = new Stan(UUID.randomUUID(),23,"Zagreb","Trg 2",23134,3);
        Stan stan2 = new Stan(UUID.randomUUID(),33,"Sirač","ULica 5",43433,1);
        Stan stan3 = new Stan(UUID.randomUUID(),53,"Bjelovar","Most 2",344343,2);

        stanovi.add(stan1);
        stanovi.add(stan2);
        stanovi.add(stan3);
    }
    @Override
    public List<Stan> getStan() {
        return stanovi;
    }



    @Override
    public void add(Stan stan) {
        stanovi.add(stan);
    }


    @Override
    public void remove(Stan stan) {
        stanovi.remove(stan);

    }


    @Override
    public boolean update(Stan stan) {
        for (Stan s : stanovi){
            if(s.getUuid() == stan.getUuid()){
                s.setKvadratura(stan.getKvadrat());
                s.setMjesto(stan.getMjesto());
                s.setUlica(stan.getUlica());
                s.setCijena(stan.getCijena());
                s.setEtaza(stan.getEtaza());
                return true;
            }
        }
        stanovi.add(stan);
        return false;
    }
}

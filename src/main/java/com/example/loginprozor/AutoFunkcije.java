package com.example.loginprozor;

import java.util.List;

public interface AutoFunkcije {

    List<Auto> getAuto();
    void add(Auto auto);

    void remove(Auto auto);

    boolean update(Auto auto);

}

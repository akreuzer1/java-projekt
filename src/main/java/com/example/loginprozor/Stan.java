package com.example.loginprozor;

import java.util.UUID;

public class Stan  extends ItemStan implements iPrikazi {
    @Override
    public String prikaziOglas(){return kvadratura+";"+ mjesto + ";" + ulica + ";" + cijena + ";" +  etaza+ ";"+ uuid;}

    public Stan(UUID uuid, int kvadratura, String mjesto, String ulica, double cijena, int etaza ){
        super(uuid,kvadratura, mjesto, ulica, cijena, etaza);
        this.kvadratura = kvadratura;
        this.mjesto = mjesto;
        this.ulica = ulica;
        this.cijena = cijena;
        this.etaza = etaza;

    }

    @Override
    public String toString(){
        return "Kvadratura="+kvadratura+"\n"+"Mjesto="+mjesto+"\n"+"Ulica="+ulica+"\n"+"Cijena="+cijena+"\n"+"Etaža="+etaza+"\n";
    }

}

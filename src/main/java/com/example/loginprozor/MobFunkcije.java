package com.example.loginprozor;

import java.util.List;

public interface MobFunkcije {
    List<Mob> getMob();
    void add(Mob mob);

    void remove(Mob mob);

    boolean update(Mob mob);
}

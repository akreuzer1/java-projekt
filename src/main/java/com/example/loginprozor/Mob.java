package com.example.loginprozor;

import java.util.UUID;

public class Mob  extends ItemMob implements iPrikazi{
    @Override
    public String prikaziOglas(){return marka+";"+ model + ";" +  cijena+ ";"+ uuid;}

    public Mob(UUID uuid, String marka, String model, double cijena){
        super(uuid,marka, model, cijena);
        this.marka = marka;
        this.model = model;

        this.cijena = cijena;

    }

    @Override
    public String toString(){
        return "Marka="+marka+"\n"+"Model="+model+"\n"+"Cijena="+cijena+"\n";
    }


}

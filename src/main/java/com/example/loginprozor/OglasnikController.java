package com.example.loginprozor;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class OglasnikController {

    AutoHandler autoDB = new AutoHandler();
    StanHandler stanDB = new StanHandler();

    MobHandler mobDB = new MobHandler();



    List<Auto> auti  = autoDB.getAuto();
    List<Stan> stanovi  = stanDB.getStan();
    List<Mob> mobiteli  = mobDB.getMob();



    @FXML
    public Label lbAuto;
    @FXML
    public Label lbStan;
    @FXML
    public TextField unosMarke_A;
    @FXML
    public TextField unosModela_A;
    @FXML
    public TextField unosGod_A;
    @FXML
    public TextField unosCijena_A;
    @FXML
    public TextField unosKm_A;
    @FXML
    public TextField unosKvad_S;
    @FXML
    public TextField unosMjesta_S;
    @FXML
    public TextField unosUl_S;
    @FXML
    public TextField unosCj_S;
    @FXML
    public TextField unosEt_S;
    @FXML
    public Label lbMob;

    @FXML
    public TextField unosMarke_M;
    @FXML
    public TextField unosModela_M;
    @FXML
    public TextField unosCj_M;




    public String au;
    public String st;
    public String mb;









    public void updateStanje(){

        prikaziAuto();
        prikaziStan();
        prikaziMob();

    }
    public void writeAutoToFile() {
        try {
            FileWriter writer = new FileWriter("auto.txt");
            for (Auto auto : auti) {
                writer.write(auto.toString() + "\n");
            }
            writer.close();
            System.out.println("Auti su uspješno upisani u txt");
        } catch (IOException e) {
            System.out.println("Dogodio se error: " + e.getMessage());
        }
    }
    public void writeStanToFile() {
        try {
            FileWriter writer = new FileWriter("stan.txt");
            for (Stan stan : stanovi) {
                writer.write(stan.toString() + "\n");
            }
            writer.close();
            System.out.println("Stanovi su uspješno upisani u txt");
        } catch (IOException e) {
            System.out.println("Dogodio se error: " + e.getMessage());
        }
    }

    public void writeMobToFile() {
        try {
            FileWriter writer = new FileWriter("Mobitel.txt");
            for (Mob mob : mobiteli) {
                writer.write(mob.toString() + "\n");
            }
            writer.close();
            System.out.println("Mobiteli su uspješno upisani u txt");
        } catch (IOException e) {
            System.out.println("Dogodio se error: " + e.getMessage());
        }
    }



    public void prikaziAuto(){
        au = "";
        for (Auto auto : auti){
            au += auto.toString() +"\n";

        }
        lbAuto.setText(au);

    }

    public void prikaziStan(){
        st = "";
        for (Stan stan : stanovi){
            st += stan.toString() +"\n";

        }
        lbStan.setText(st);

    }
    public void prikaziMob(){
        mb= "";
        for (Mob mob : mobiteli){
            mb += mob.toString() +"\n";

        }
        lbMob.setText(mb);

    }
    public void Insert(Add unos){ unos.dodaj(); }
    public void InsertStan(AddS unosS){ unosS.dodajS(); }
    public void InsertMob(AddM unosM){ unosM.dodajM(); }
    public void Remove(Remove brisi){ brisi.makni();};
    public void RemoveM(RemoveM brisiM){ brisiM.makniM();};

    public void RemoveS(RemoveS brisiS){ brisiS.makniS();};



    public void unesiAuto(){
        Auto auto = new Auto(UUID.randomUUID(),unosMarke_A.getText(),unosModela_A.getText(),Double.parseDouble(unosKm_A.getText()),Integer.parseInt(unosGod_A.getText()), Integer.parseInt(unosCijena_A.getText()));

        Add dodajAuto = () -> {
            autoDB.add(auto);
        };
        Insert(dodajAuto);
        updateStanje();
    }
    public void unesiMob(){
        Mob mob = new Mob(UUID.randomUUID(),unosMarke_M.getText(),unosModela_M.getText(), Integer.parseInt(unosCj_M.getText()));

        AddM dodajMob = () -> {
            mobDB.add(mob);
        };
        InsertMob(dodajMob);
        updateStanje();
    }

    public void unesiStan(){
        Stan stan = new Stan(UUID.randomUUID(),Integer.parseInt(unosKvad_S.getText()) ,unosMjesta_S.getText(),unosUl_S.getText(),Double.parseDouble(unosCj_S.getText()), Integer.parseInt(unosEt_S.getText()));

        AddS dodajStan = () -> {
            stanDB.add(stan);
        };
        InsertStan(dodajStan);
        updateStanje();
    }


    public void obrisiAuto(){
        for (Auto auto : auti){
            if (unosMarke_A.getText().equals((auto.getMarka()))){
                Remove makni = () -> {
                    autoDB.remove(auto);
                };
                Remove(makni);
                updateStanje();
            }
        }


    }
    public void obrisiStan(){
        for (Stan stan : stanovi){
            if (unosMjesta_S.getText().equals((stan.getMjesto()))){

                RemoveS makniS = () -> {
                    stanDB.remove(stan);
                };

                RemoveS(makniS);

                updateStanje();
            }
        }
    }
    public void obrisiMob(){
        for (Mob mob : mobiteli){
            if (unosMarke_M.getText().equals((mob.getMarka()))){
                RemoveM makniM = () -> {
                    mobDB.remove(mob);
                };
                RemoveM(makniM);
                updateStanje();
            }
        }
    }

    public void updateAuto(){
        for (Auto auto : auti){
            if (unosMarke_A.getText().equals(auto.getMarka())){
                Auto novi = new Auto(auto.getUuid(),unosMarke_A.getText(),unosModela_A.getText(),Integer.parseInt(unosKm_A.getText()),Double.parseDouble(unosGod_A.getText()), Integer.parseInt(unosCijena_A.getText()));
                autoDB.update(novi);
            }
        }


        updateStanje();
    }

    public void updateMob(){
        for (Mob mob : mobiteli){
            if (unosMarke_M.getText().equals(mob.getMarka())){
                Mob novi = new Mob(mob.getUuidM(),unosMarke_M.getText(),unosModela_M.getText(),Double.parseDouble(unosCj_M.getText()));
                mobDB.update(novi);
            }
        }


        updateStanje();
    }


    public void updateStan(){
        for (Stan stan : stanovi){
            if (unosMjesta_S.getText().equals(stan.getMjesto())){
                Stan novi =  new Stan(UUID.randomUUID(),Integer.parseInt(unosKvad_S.getText()),unosMjesta_S.getText(),unosUl_S.getText(),Double.parseDouble(unosCj_S.getText()), Integer.parseInt(unosEt_S.getText()));
                stanDB.update(novi);
            }
        }
        updateStanje();
    }



}

package Utilities;

public class GeneralTaxingConstants {
    public static final double TAX_STUDENT = 0.1;
    public static final double TAX_WORKER = 0.2;
    public static final double TAX_PENSIONER = 0.05;
}
